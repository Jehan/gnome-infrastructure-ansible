---
- name: add authorized keys for root
  copy: src=authorized_keys dest=/root/.ssh/authorized_keys mode=0600 owner=root group=root

- include_tasks: ubuntu.yml
  when: ansible_distribution == "Ubuntu"

- include_tasks: debian.yml
  when: ansible_distribution == "Debian"

- include_tasks: redhat.yml
  when: ansible_os_family == "RedHat"
  tags: selinux

- name: configure docker
  template: src=dockerd.json.j2 dest=/etc/docker/daemon.json
  notify: restart docker
  tags: docker

- name: start and enable docker
  service: name=docker state=started enabled=true
  tags: docker

- name: create gitlab-runner config directory
  file:
    path: /etc/gitlab-runner
    state: directory
    mode: '0700'

- name: configure gitlab-runner
  template:
    src: config.toml.j2
    dest: /etc/gitlab-runner/config.toml
  tags: gitlab-runner

- name: increase arp table size
  copy: src=increase-arp-table-size.conf dest=/etc/sysctl.d/increase-arp-table-size.conf mode=0644 owner=root group=root
  notify: reload sysctl

- name: increase number of file descriptors
  copy: src=increase-file-max.conf dest=/etc/sysctl.d/increase-file-max.conf mode=0644 owner=root group=root
  notify: reload sysctl
  tags: sysctl

- name: create docuum container
  docker_container:
    name: docuum
    image: stephanmisc/docuum:latest
    state: present
    restart_policy: on-failure
    command: "--threshold '{{ gitlab_runner_docuum_threshold }}'"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
  tags: docuum
  when: gitlab_runner_enable_docuum and gitlab_runner_docuum_threshold is defined

- name: start docuum
  docker_container:
    name: docuum
    state: started
  tags: docuum
  when: gitlab_runner_enable_docuum and gitlab_runner_docuum_threshold is defined

- name: 'Remove old containers and volumes on schedule'
  cron:
    name: "Prune old containers and volumes"
    special_time: daily
    job: "docker system prune -af --filter label=com.gitlab.gitlab-runner.managed=true"

- name: Make sure /dev/kvm is root:root (0660)
  copy: src=00-kvm.rules dest=/etc/udev/rules.d/00-kvm.rules mode=0644 owner=root group=root
  notify: reload udevadm
