---
- name: create system group
  group:
    name: postgres_exporter
    system: true
    state: present

- name: create system user
  user:
    name: postgres_exporter
    system: true
    shell: "/sbin/nologin"
    group: postgres_exporter
    createhome: false

- name: create download directory
  file:
    path: /root/.ansible/tmp/postgres-exporter
    state: directory

- name: download {{ postgres_exporter_version }} for {{ ansible_architecture }}
  get_url:
    url: "https://github.com/wrouesnel/postgres_exporter/releases/download/v{{ postgres_exporter_version }}/{{ postgres_exporter_tarball_basename }}.tar.gz"
    dest: "/root/.ansible/tmp/postgres-exporter/{{ postgres_exporter_tarball_basename }}.tar.gz"
    checksum: "{{ postgres_exporter_checksum }}"

- name: unpack binaries
  unarchive:
    src: "/root/.ansible/tmp/postgres-exporter/{{ postgres_exporter_tarball_basename }}.tar.gz"
    dest: /root/.ansible/tmp/postgres-exporter/
    creates: "/root/.ansible/tmp/postgres-exporter/{{ postgres_exporter_tarball_basename }}/postgres_exporter"
    remote_src: true
  check_mode: false

- name: install binaries
  copy:
    src: "/root/.ansible/tmp/postgres-exporter/{{ postgres_exporter_tarball_basename }}/{{ item }}"
    dest: "/usr/local/bin/{{ item }}"
    mode: 0755
    remote_src: true
  with_items:
    - postgres_exporter
  notify: restart postgres-exporter

- name: install queries config
  copy:
    src: queries.yaml
    dest: /etc/postgres_exporter.queries.yml
    mode: 0755
  notify: restart postgres-exporter

- name: install systemd unit
  template:
    src: postgres-exporter.service.j2
    dest: /etc/systemd/system/postgres-exporter.service
    owner: root
    group: root
    mode: 0644
  notify: restart postgres-exporter

- name: start and enable systemd unit
  systemd:
    name: postgres-exporter
    daemon-reload: yes
    state: started
    enabled: yes
