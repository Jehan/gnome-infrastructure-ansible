---
gitlab_runner_concurrent: 1
gitlab_runner_check_interval: 0

gitlab_runner_runners:
  - name: "codethink-arm-4.zt.flathub.org"
    output_limit: 32768
    url: "https://gitlab.gnome.org/"
    token: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          36396165633038393866666234373866336532306264623533313532333237313432666130646164
          6434623165316436363165666432343163356635656139350a383132636439663563663833303263
          63363565356439313161653533383763343262316164333064646565663733633935653030353864
          6135393239623339380a393039323234353062393933313131643337356664383966386132613133
          66376438386562373439336365396165343038393063353561653136366434363734
    executor: "docker"
    seccomp: "buildah"
    docker:
      tls_verify: "false"
      image: "\"fedora:latest\""
      disable_entrypoint_overwrite: "false"
      oom_kill_disable: "false"
      volumes: '["/cache"]'
      disable_cache: "true"
      shm_size: 0
      privileged: "false"
    cache:
      Type: "\"s3\""
      Shared: "true"
    cache.s3:
      ServerAddress: "{{ gitlab_runner_cache_endpoint }}"
      AccessKey: "{{ gitlab_runner_cache_access_key }}"
      SecretKey: "{{ gitlab_runner_cache_secret_key }}"
      BucketName: "{{ gitlab_runner_cache_bucket_name }}"
      BucketLocation: "{{ gitlab_runner_cache_bucket_region }}"
